public class Alimento{
	String tamaño;
	String forma;
	String tipo;
	String tipoAlimento;

	public Alimento tamaño(String tamaño){
		this.tamaño = tamaño;
		return this;
	}

	public Alimento forma(String forma){
		this.forma = forma;
		return this;
	}

	public Alimento tipo(String tipo){
		this.tipo = tipo;
		return this;
	}

	public Alimento tipoAlimento(String tipoAlimento){
		this.tipoAlimento = tipoAlimento;
		return this;
	}

	public String toString(){
		return tipoAlimento+"("+"Tipo:"+tipo+" Forma:"+forma+" Tamaño:"+tamaño+")";
	}
}