abstract class Receta{
	protected Alimento alimento;

	public void nuevoAl(){
		alimento = new Alimento();
	}

	public Alimento getAlimento(){
		return alimento;
	}

	public abstract void cocinar();
	public abstract void servir();
}