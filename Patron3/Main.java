import java.util.Scanner;

public class Main{
	public static void main(String args[]){
		System.out.println("Escribe Pizza o Pastel");
		Scanner sc = new Scanner(System.in);
		String eleccion = sc.next();
		Restaurante rest = new Restaurante();
		Receta receta;

		if(eleccion.equals("Pizza")){
			receta = new RecetaPizza();
		}else{
			receta = new RecetaPastel();
		}
		rest.establecerReceta(receta);
		rest.cocinarAlimento();

		Alimento comida = rest.getAlimento();

		System.out.println(comida.toString());
	}
}