public class Restaurante{
	private Receta receta;
	
	public void establecerReceta(Receta r){
		this.receta=r;
	}

	public Alimento getAlimento(){
		return receta.getAlimento();
	}

	public void cocinarAlimento(){
		receta.nuevoAl();
		receta.cocinar();
		receta.servir();
	}
}