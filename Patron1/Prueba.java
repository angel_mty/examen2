public class Prueba{
	public static void main(String args[]){
		Forma forma = Forma.getFormaInstance("circulo","rojo");
		Object fig = "";
		if(forma.getFigura().equals("circulo")){
			 fig = new Circulo(2,"circulo");
		}else {
			fig = new Cuadrado(2,"cuadrado");
		}

		System.out.println(fig.toString());
	}
}