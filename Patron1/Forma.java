public class Forma{
	private static Forma forma;
	private String figura = "";
	private String color = "";


	private Forma(String figura, String color){
		this.figura = figura;
		this.color = color;
	}

	public static Forma getFormaInstance(String figura, String color){
		if(forma==null){
			forma = new Forma(figura, color);
		}
		return forma;
	}

	public String getFigura(){
		return this.figura;
	}

	public String getColor(){
		return this.color;
	}

    
}

class Circulo {
	String figura ="Circulo";
	String color;
	int radio;

	public Circulo(int radio,String figura){
		this.figura = figura;
		this.radio = radio;
	}

	public String toString(){
		return figura+" "+radio;
	}

}

class Cuadrado {
	String figura ="Cuadrado";
	String color;
	int lado;

	public Cuadrado(int lado, String figura){
		this.figura = figura;
		this.lado = lado;
	}

	public String toString(){
		return figura+" "+lado;
	}

}



