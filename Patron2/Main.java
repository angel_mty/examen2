//Miguel Angel Montoya Celestion

public class Main {
    public static void main(String args[]){
        Periodico periodico = new Periodico();
        Subscriptor1 sub1 = new Subscriptor1();
        Subscriptor2 sub2 = new Subscriptor2();
        String mail = "Sea subido el periodico de hoy";
        
        periodico.addObserver(sub1);
         periodico.setMail(mail);
        System.out.println("Agregar otro subscriptor");
        periodico.addObserver(sub2);
        periodico.setMail(mail);
        System.out.println("Eliminando subscriptor");
        periodico.deleteObserver(sub1);
       	periodico.setMail(mail);

  
        
     
    }
}
