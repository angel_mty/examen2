import java.util.Observable;


public class Periodico extends Observable{
	String mail = "sin mensaje";

	public Periodico(){
		mail = "Sin mensaje";
	}

	public void setMail(String m){
		mail = m;
		setChanged();
		notifyObservers(mail);
	}

}